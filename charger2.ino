#include <Button.h>

//----------------------------------------------------------------------------------------------------
//  ARDUINO MPPT SOLAR CHARGE CONTROLLER (Version-3) 
//  Author: Debasish Dutta/deba168
//          www.opengreenenergy.com
//
//  This code is for an arduino Nano based Solar MPPT charge controller.
//  This code is a modified version of sample code from www.timnolan.com
//  dated 08/02/2015
//
//  Mods by Aplavins 01/07/2015
//
//  Size batteries and panels appropriately. Larger panels need larger batteries and vice versa.

////  Specifications :  //////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                                                                            //
//    1.Solar panel power = 50W                                            
                                                                                                                            //
//    2.Rated Battery Voltage= 12V ( lead acid type )

//    3.Maximum current = 5A                                                                                                //

//    4.Maximum load current =10A                                                                                            //

//    5. In put Voltage = Solar panel with Open circuit voltage from 17 to 25V                                               //

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>


#include <TimerOne.h>               // using Timer1 library from http://www.arduino.cc/playground/Code/Timer1
#include <LiquidCrystal.h>      // using the LCD I2C Library from https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads
#include <Wire.h>  
#include <Button.h> 
//----------------------------------------------------------------------------------------------------------
 
//////// Arduino pins Connections//////////////////////////////////////////////////////////////////////////////////

// A0 - Voltage divider (solar)
// A1 - ACS 712 Out
// A2 - Voltage divider (battery)
// A4 - LCD SDA
// A5 - LCD SCL
// D2 - ESP8266 Tx
// D3 - ESP8266 Rx through the voltage divider
// D5 - LCD back control button
// D6 - Load Control 
// D8 - 2104 MOSFET driver SD
// D9 - 2104 MOSFET driver IN  
// D11- Green LED
// D12- Yellow LED
// D13- Red LED

// Full scheatic is given at http://www.instructables.com/files/orig/F9A/LLR8/IAPASVA1/F9ALLR8IAPASVA1.pdf
#define NUM_CELLS 6


#define ADC_FULL_COUNT  1023
#define ADC_VREF_NOM    3.0
#define GVCOUT          0.6
#define GVSENSE           4

#define VCOUT_PIN A0
#define VIOUT_PIN A1
#define THERM_PIN  A2

#define Vol_avg_count 20

#define I2C_GROUP_ADDR  0x4

#define STATUS          (I2C_GROUP_ADDR << 3) + 0x00    // Read & Write
#define CELL_CTL        (I2C_GROUP_ADDR << 3) + 0x01    // Read & Write
#define BAL_CTL         (I2C_GROUP_ADDR << 3) + 0x02    // Read & Write
#define CONFIG_1        (I2C_GROUP_ADDR << 3) + 0x03    // Read & Write
#define CONFIG_2        (I2C_GROUP_ADDR << 3) + 0x04    // Read & Write
#define POWER_CTL       (I2C_GROUP_ADDR << 3) + 0x05    // Read & Write
#define CHIP_ID         (I2C_GROUP_ADDR << 3) + 0x07    // Read Only
#define VREF_CAL        (I2C_GROUP_ADDR << 3) + 0x10    // Read Only
#define VC1_CAL         (I2C_GROUP_ADDR << 3) + 0x11    // Read Only
#define VC2_CAL         (I2C_GROUP_ADDR << 3) + 0x12    // Read Only
#define VC3_CAL         (I2C_GROUP_ADDR << 3) + 0x13    // Read Only
#define VC4_CAL         (I2C_GROUP_ADDR << 3) + 0x14    // Read Only
#define VC5_CAL         (I2C_GROUP_ADDR << 3) + 0x15    // Read Only
#define VC6_CAL         (I2C_GROUP_ADDR << 3) + 0x16    // Read Only
#define VC_CAL_EXT_1    (I2C_GROUP_ADDR << 3) + 0x17    // Read Only
#define VC_CAL_EXT_2    (I2C_GROUP_ADDR << 3) + 0x18    // Read Only
#define VREF_CAL_EXT    (I2C_GROUP_ADDR << 3) + 0x1B    // Read Only

// =============================================================================
// Define fault thresholds
//
// The fault thresholds are transformed using function like macros. The trans-
// formation reduces code size by avoiding floating point arithmetic and
// division.
//
// VC_TRANS(x)  : Transforms voltage threshold. 'x' is in Volts.
// VI_TRANS(x)  : Transforms current threshold. 'x' is in Volts.
// VT_TRANS(x)  : Transforms temperature threshold. 'x' is in Volts.
// =============================================================================
#define VC_TRANS(x)     ((unsigned int) (x * GVCOUT * ADC_FULL_COUNT * 1e6 / 65536))
#define VI_TRANS(x)     ((unsigned int) (x * GVSENSE * ADC_FULL_COUNT * 1e3 / 256))
#define VT_TRANS(x)     ((unsigned int) (x * ADC_FULL_COUNT * 1e3 / 256))

#define OV_THRESH       4.250   // Over voltage threshold in Volts
#define UV_THRESH       2.500   // Under voltage threshold in Volts
#define OC_THRESH       0.125   // Over current threshold in Volts
#define OT_THRESH       0.700   // Over temperature threshold in Volts

long Cell_Vol[6];
long StackVoltage;
long batCurrent;
unsigned int adc_sensen;        // Store value of SENSEN measurement
unsigned int adc_sensep;        // Store value of SENSEN measurement
  unsigned char ov_flag,uv_flag;  // Fault flags

int adc_data;
typedef struct {
    unsigned char data;
    unsigned char error;              
} i2c_trans;

///////// Definitions /////////////////////////////////////////////////////////////////////////////////////////////////
#define FAN_PIN 6
float MAX_BAT_CURRENT = 7.0;
#define SOL_VOLTS_CHAN A1                  // defining the adc channel to read solar volts
#define SOL_AMPS_CHAN A2                    // Defining the adc channel to read solar amps
#define BAT_VOLTS_CHAN A1                   // defining the adc channel to read battery volts
#define AVG_NUM 8                          // number of iterations of the adc routine to average the adc readings
#define BAT_AMPS_CHAN A2
// ACS 712 Current Sensor is used. Current Measured = (5/(1024 *0.185))*ADC - (2.5/0.185) 

#define SOL_AMPS_SCALE  0.0048828        // the scaling value for raw adc reading to get solar amps   // 5/(1024*0.185)
#define SOL_VOLTS_SCALE 0.09179      // the scaling value for raw adc reading to get solar volts  // (5/1024)*(R1+R2)/R2 // R1=100k and R2=20k
#define BAT_VOLTS_SCALE 0.03225        // the scaling value for raw adc reading to get battery volts 

#define PWM_PIN 9                          // the output pin for the pwm (only pin 9 avaliable for timer 1 at 50kHz)
#define PWM_ENABLE_PIN 8                   // pin used to control shutoff function of the IR2104 MOSFET driver (hight the mosfet driver is on)

 

#define TURN_ON_MOSFETS digitalWrite(PWM_ENABLE_PIN, HIGH)      // enable MOSFET driver
#define TURN_OFF_MOSFETS digitalWrite(PWM_ENABLE_PIN, LOW)      // disable MOSFET driver

#define ONE_SECOND 50000                   // count for number of interrupt in 1 second on interrupt period of 20us

#define MIN_SOLAR_VOLTS 25
#define MAX_BAT_VOLTS 26                 // we don't want the battery going any higher than this
#define BAT_FLOAT 26.7                   // battery voltage we want to stop charging at
#define LVD 11.5                           // Low voltage disconnect
#define OFF_NUM 9                          // number of iterations of off charger state
#define PULLUP true        //To keep things simple, we use the Arduino's internal pullup resistor.
#define INVERT true 
#define DEBOUNCE_MS 50  
 // LiquidCrystal lcd(7, 6, 12, 4, 3, 2);
LiquidCrystal_I2C lcd(0x3F,16,2);
//------------------------------------------------------------------------------------------------------
//Defining led pins for indication
#define LED_RED 5
#define LED_GREEN 12
#define LED_YELLOW 13
//-----------------------------------------------------------------------------------------------------
// Defining load control pin
#define LOAD_PIN 3      // pin-2 is used to control the load
  
//-----------------------------------------------------------------------------------------------------
// Defining lcd back light pin
#define BACK_LIGHT_PIN 5       // pin-5 is used to control the lcd back light
//------------------------------------------------------------------------------------------------------
/////////////////////////////////////////BIT MAP ARRAY//////////////////////////////////////////////////
//-------------------------------------------------------------------------------------------------------
byte solar[8] = //icon for termometer
{
  0b11111,
  0b10101,
  0b11111,
  0b10101,
  0b11111,
  0b10101,
  0b11111,
  0b00000
};

byte battery[8]=
{
  0b01110,
  0b11011,
  0b10001,
  0b10001,
  0b11111,
  0b11111,
  0b11111,
  0b11111,
};

byte _PWM [8]=
{
  0b11101,
  0b10101,
  0b10101,
  0b10101,
  0b10101,
  0b10101,
  0b10101,
  0b10111,
};
//-------------------------------------------------------------------------------------------------------
//Button myBtn1(7, PULLUP, INVERT, DEBOUNCE_MS); 
// global variables
float bat_amps;
float output_watts,efficiency;
boolean batteryCharged = false;
float sol_amps;                       // solar amps 
float sol_volts;                      // solar volts 
float bat_volts;                      // battery volts 
double sol_watts;                      // solar watts
float old_sol_watts = 0;              // solar watts from previous time through ppt routine
float loaded_volts = 0;               // variable for storing battery voltage under load
float deltaV = 0;                     // vairable for storing the difference of voltage when under load
unsigned int seconds = 0;             // seconds from timer routine
unsigned int prev_seconds = 0;        // seconds value from previous pass
unsigned int interrupt_counter = 0;   // counter for 20us interrrupt
unsigned long lastUpdatetime = 0,lastSetTime = 0,lastSleepTime = 0,lastMAHUpdate = 0,lastUpdatetimeBack =0;               // variable to store time the back light control button was pressed in millis
int pulseWidth = 400;                 // pwm duty cycle 0-1024
int pwm = 0;                          // mapped value of pulseWidth in %
int back_light_pin_State = 0;         // variable for storing the state of the backlight button
int load_status = 0;                  // variable for storing the load output state (for writing to LCD)
int prev_load_status = 0;             // to see when the load has been turned on
int trackDirection = 10;               // step amount to change the value of pulseWidth used by MPPT algorithm
  int lcdCounter = 0;
enum charger_mode {no_battery, sleep, bulk, Float, error} charger_state;  // enumerated variable that holds state for charger state machine
// set the LCD address to 0x27 for a 20 chars 4 line display
// Set the pins on the I2C chip used for LCD connections:
//                    addr, en,rw,rs,d4,d5,d6,d7,bl,blpol
  // Set the LCD I2C address
  unsigned int displaymAh =0;
    double calmAh=0;

boolean autoTrack=true;
boolean ledState = false;

//------------------------------------------------------------------------------------------------------
// This routine is automatically called at powerup/reset
//------------------------------------------------------------------------------------------------------

void setup()                           // run once, when the sketch starts
{

  pinMode(FAN_PIN, OUTPUT);            // sets the digital pin as output
  pinMode(LED_GREEN, OUTPUT);          // sets the digital pin as output
  pinMode(LED_YELLOW, OUTPUT);         // sets the digital pin as output
  pinMode(PWM_ENABLE_PIN, OUTPUT);     // sets the digital pin as output
  TURN_OFF_MOSFETS;                    // turn off MOSFET driver chip
  Timer1.initialize(20);               // initialize timer1, and set a 20uS period
  Timer1.attachInterrupt(callback);    // attaches callback() as a timer overflow interrupt
 Serial.begin(9600);                  // open the serial port at 38400 bps:
  charger_state = sleep;               // start with charger state as sleep
  pinMode(BACK_LIGHT_PIN, INPUT);      // backlight on button
  pinMode(FAN_PIN,OUTPUT);            // output for the LOAD MOSFET (LOW = on, HIGH = off)
  digitalWrite(LOAD_PIN,HIGH);         // default load state is OFF
  // lcd.begin(20,4);                     // initialize the lcd for 16 chars 2 lines, turn on backlight
  // lcd.noBacklight();                   // turn off the backlight
  // lcd.createChar(1,solar);             // turn the bitmap into a character
  // lcd.createChar(2,battery);           // turn the bitmap into a character
  // lcd.createChar(3,_PWM);              // turn the bitmap into a character
  Wire.begin();
  lcd.init();                      // initialize the lcd 
 BMS_init();
  // Print a message to the LCD.
  lcd.backlight();
  lastUpdatetimeBack = millis();
  lcdCounter++;
}

//------------------------------------------------------------------------------------------------------
// Main loop
//------------------------------------------------------------------------------------------------------
void loop()                         
{
 // myBtn1.read(); 
  read_data();                         // read data from inputs
  updateCellVoltages();
  mode_select();                       // select the charging state
   if(charger_state == bulk)
    batteryCharged = false;
  set_charger();                       // run the charger state machine
  print_data();                        // print data
  lcd_timer();
  //load_control();                      // control the connected load
 // led_output();                        // led indication
  //lcd_display();                       // lcd display
//  if (myBtn1.wasReleased()) { 
//     lastUpdatetimeBack = millis(); 
//    lcdCounter++;
//    lcd.clear();
//    if(lcdCounter == 2)
//      lcdCounter = 0;
//  }

  if(bat_amps>3)
    digitalWrite(FAN_PIN, HIGH);
  else  
    digitalWrite(FAN_PIN, LOW);
 

  if(displaymAh % 5000 == 0){
    disable_charger();
    pulseWidth = 400;
    delay(2000);
    read_data();
    if(bat_volts >= BAT_FLOAT)
      batteryCharged = true;
    else
      batteryCharged = false;

    calmAh= calmAh + 1;

  }

  if(((millis() - lastUpdatetimeBack) < 30000) ){     
    lcd.backlight();
    }
    else{
    lcd.noBacklight();
    }



  if(((millis() - lastMAHUpdate) > 100) ){     

    calmAh += bat_amps*0.1/3600*1000;
    displaymAh = (int) calmAh;
    lastMAHUpdate = millis();

    }

if((millis() - lastSetTime) > 1500) {

  if(bat_amps > (MAX_BAT_CURRENT-0.3)){
    if(bat_volts < 16)
      MAX_BAT_CURRENT = MAX_BAT_CURRENT + 0.3;
    else
      MAX_BAT_CURRENT = MAX_BAT_CURRENT - 0.3;

    if(MAX_BAT_CURRENT < 2)
      MAX_BAT_CURRENT = 2;
    else if(MAX_BAT_CURRENT > 14)
      MAX_BAT_CURRENT = 14;
    lastSetTime = millis();
  }
 } 

}

void BMS_init(){

  i2c_trans i2c_read_struct, i2c_write_struct;

analogReference(EXTERNAL);
  signed char offset_corr[7];
  signed char gain_corr[7];
  
  unsigned int vref_corr;         // Corrected VREF x 1000
  
  volatile unsigned int adc_data; // ADC data from conversion
  

  unsigned int index;             // General purpose index


  if (i2c_write_and_verify(CONFIG_1, 0x00, &i2c_write_struct)) error_trap();
  // Set VREF_NOM = 1.5 V and GVCOUT = 0.3 (REF_SEL = 1)
  if (i2c_write_and_verify(CONFIG_2, 0x01, &i2c_write_struct)) error_trap();
  // Enable VREF, VCOUT and VIOUT.
  if (i2c_write_and_verify(POWER_CTL, 0x0D, &i2c_write_struct)) error_trap();

  for (index = 0; index < 7; index++) {
      if (i2c_read( VREF_CAL | index, &i2c_read_struct)) error_trap();
      offset_corr[index] = i2c_read_struct.data >> 4;
      gain_corr[index]   = i2c_read_struct.data & 0x0F;
  }

 // Get the 2 msb for offset and 1 msb for gain correction of VREF
  if (i2c_read(VREF_CAL_EXT, &i2c_read_struct)) error_trap();
  // Shift in msb and sign extend
  offset_corr[0] |= (((i2c_read_struct.data & 0x06) << 3) ^ 0x20) - 0x20;
  gain_corr[0]   |= (((i2c_read_struct.data & 0x01) << 4) ^ 0x10) - 0x10;
      
  // Get the msb for offset and gain corrections for VC1 and VC2
  if (i2c_read(VC_CAL_EXT_1, &i2c_read_struct)) error_trap();
  // Shift in msb and sign extend
  offset_corr[1] |= (((i2c_read_struct.data & 0x80) >> 3) ^ 0x10) - 0x10;
  gain_corr[1]   |= (((i2c_read_struct.data & 0x40) >> 2) ^ 0x10) - 0x10;
      
  offset_corr[2] |= (((i2c_read_struct.data & 0x20) >> 1) ^ 0x10) - 0x10;
  gain_corr[2]   |= (((i2c_read_struct.data & 0x10)) ^ 0x10) - 0x10;
          
  // Get the msb for offset and gain corrections for VC3, VC4, VC5, VC6
  if (i2c_read(VC_CAL_EXT_2, &i2c_read_struct)) error_trap();
  // Shift in msb and sign extend
  offset_corr[3] |= (((i2c_read_struct.data & 0x80) >> 3) ^ 0x10) - 0x10;
  gain_corr[3]   |= (((i2c_read_struct.data & 0x40) >> 2) ^ 0x10) - 0x10;
  
  offset_corr[4] |= (((i2c_read_struct.data & 0x20) >> 1) ^ 0x10) - 0x10;
  gain_corr[4]   |= (((i2c_read_struct.data & 0x10)) ^ 0x10) - 0x10;
  
  offset_corr[5] |= (((i2c_read_struct.data & 0x08) << 1) ^ 0x10) - 0x10;
  gain_corr[5]   |= (((i2c_read_struct.data & 0x04) << 2) ^ 0x10) - 0x10;
  
  offset_corr[6] |= (((i2c_read_struct.data & 0x02) << 3) ^ 0x10) - 0x10;
  gain_corr[6]   |= (((i2c_read_struct.data & 0x01) << 4) ^ 0x10) - 0x10;

  vref_corr = ADC_VREF_NOM * (1000L + gain_corr[0]) + offset_corr[0];
  Serial.println(vref_corr);
  // Before starting loop, SENSEN is measured. When the current amplifier is
  // turned on, it must charge up to the zero current level (~ 2V). If the
  // output filter is large, it may take awhile for VIOUT to reach a steady
  // value. Here, we delay ~2 sec before measurement to accomodate a very 
  // large filter. Smaller delays can be used for smaller filters.

  for (int i = 0; i < NUM_CELLS; ++i)
  {
    Serial.print(offset_corr[i],BIN);
    Serial.print(" ");
    Serial.println(gain_corr[i]+1,BIN);


  }

  delay(1000);

  adc_sensen = analogRead(VIOUT_PIN);
    Serial.print("adc sensen ");
   Serial.println(adc_sensen);
  // Switch bq76925 to SENSEP for all other current current measurements
  if (i2c_write_and_verify(CONFIG_1, 0x04, &i2c_write_struct)) error_trap();



        


        // Measure cell voltages
        // ---------------------
        // Initialize fault flags
        ov_flag = 0;
        uv_flag = 0;
        
        // Measure VCn via VCOUT for n = 1 to 6
        // Cycle through all the cell voltage channels
        for (index = 0; index < 6; index++) {
            // Setup cell MUX in bq76925 to select desired cell input.
            if (i2c_write_and_verify(CELL_CTL, 0x10 | index, &i2c_write_struct)) error_trap();
            
            // Delay 32 ms before taking measurement. Delay takes into account
            // the time required to charge or discharge any capacitance in the
            // output filter on VCOUT and should be adjusted according to 
            // the max voltage swing and circuit values.
            delay(50);
            
            // Corrected VCOUT voltage = 
            //
            //       ADC Count              offset_corr          gain_corr
            //  ( ---------------- x VREF + ----------- ) x (1 + ---------)
            //    Full-scale count             1000                1000
            //
            // Multiply through by Full-scale count x 1E6. Recall that
            // 1000 x VREF = vref_corr. Divide by 65536 to get 16-bit result.
            adc_data = ((long) analogRead(VCOUT_PIN) * vref_corr + ADC_FULL_COUNT * offset_corr[index+1]) \
                       * (1000L + gain_corr[index+1]) >> 16;

            double tempCellVoltage = (analogRead(VCOUT_PIN) / ADC_FULL_COUNT * vref_corr) * (1000) /0.6;

            Serial.print(tempCellVoltage);
            Serial.print(" ");
            // Check for under voltage
            if (adc_data < VC_TRANS(UV_THRESH)) uv_flag = 1;
            // Check for over voltage
            if (adc_data > VC_TRANS(OV_THRESH)) ov_flag = 1;
        }
        Serial.println(" ");
        

        


}
//------------------------------------------------------------------------------------------------------
// This routine reads and averages the analog inputs for this system, solar volts, solar amps and 
// battery volts. 
//------------------------------------------------------------------------------------------------------
int read_adc(int channel){
  
  int sum = 0;
  int temp;
  int i;
  
  for (i=0; i<AVG_NUM; i++) {          // loop through reading raw adc values AVG_NUM number of times  
    temp = analogRead(channel);        // read the input pin  
    sum += temp;                       // store sum for averaging
    delayMicroseconds(50);             // pauses for 50 microseconds  
  }
  return(sum / AVG_NUM);               // divide sum by AVG_NUM to get average and return it
}

//------------------------------------------------------------------------------------------------------
// This routine reads all the analog input values for the system. Then it multiplies them by the scale
// factor to get actual value in volts or amps. 
//------------------------------------------------------------------------------------------------------


void read_data(void) {
  old_sol_watts = sol_watts;                                       // save the previous value of sol watts
  
  sol_amps = (read_adc(SOL_AMPS_CHAN) - 773)/1023.0*ADC_VREF_NOM/0.066;    // input of solar amps
 // Serial.println(sol_amps);
  bat_amps = (read_adc(BAT_AMPS_CHAN) * SOL_AMPS_SCALE -2.5)*10;
  sol_volts = read_adc(SOL_VOLTS_CHAN) * SOL_VOLTS_SCALE;          // input of solar volts 
  bat_volts = StackVoltage/1000.0;          // input of battery volts 
                              // calculations of solar watts  
  if(sol_volts<0)
    sol_volts=0;
  if(bat_volts<0)
    bat_volts=0;
  //if(sol_amps<0)
   // sol_amps = 0;
  if(bat_amps<0)
    bat_amps = 0;   
  sol_watts = sol_amps * sol_volts ;   
  output_watts = bat_volts*bat_amps;
  efficiency = output_watts/sol_watts*100;
}

//------------------------------------------------------------------------------------------------------
// This is interrupt service routine for Timer1 that occurs every 20uS.
//
//------------------------------------------------------------------------------------------------------
void callback()
{
  interrupt_counter++;
  if (interrupt_counter >= ONE_SECOND) {        // increment interrupt_counter until one second has passed
    interrupt_counter = 0;                       // reset the counter
    seconds++;                                   // then increment seconds counter
  }

} 

void mode_select(){
  if (bat_volts < 8.0) charger_state = no_battery ;                                           // If battery voltage is below 10, there is no battery connected or dead / wrong battery
  else if (bat_volts > MAX_BAT_VOLTS) charger_state = error;                                   // If battery voltage is over 15, there's a problem
  else if ((bat_volts > 7.0) && (bat_volts < MAX_BAT_VOLTS) && (sol_volts >= MIN_SOLAR_VOLTS)){  // If battery voltage is in the normal range and there is light on the panel
    if (bat_volts >= (BAT_FLOAT) && batteryCharged == true) charger_state = Float;                                   // If battery voltage is above 13.5, go into float charging
    else charger_state = bulk;                                                                 // If battery voltage is less than 13.5, go into bulk charging
  }
  else if (sol_volts < MIN_SOLAR_VOLTS ){                                                         // If there's no light on the panel, go to sleep
    charger_state = sleep;
  }
}

void set_charger(void) {
    
  switch (charger_state){                                                                     // skip to the state that is currently set
    
    case no_battery:                                                                          // the charger is in the no battery state
      pulseWidth = 300;   
      disable_charger();                                                                      // Disable the MOSFET driver
      break;
    
    case sleep:                                                                               // the charger is in the sleep state
      pulseWidth = 300;                                                                       // set the duty cycle to 50% for when it comes on again
      disable_charger();                                                                      // Disable the MOSFET driver
      break;
      
    case bulk:                                                                                // the charger is in the bulk state
      PerturbAndObserve2();                                                                    // run the MPPT algorithm
      enable_charger();                                                                       // If battery voltage is below 13.6 enable the MOSFET driver
      break;
      
    case Float:                                                                               // the charger is in the float state, it uses PWM instead of MPPT
      pulseWidth = 300;                                                                      // set the duty cycle to maximum (we don't need MPPT here the battery is charged)
    //  if (bat_volts < BAT_FLOAT) enable_charger();                                            // If battery voltage is below 13.6 enable the MOSFET driver
    // else 
      disable_charger();                                                                 // If above, disable the MOSFET driver
      break;
      
    case error:          
      pulseWidth = 300;                                                                       // if there's something wrong
      disable_charger();                                                                      // Disable the MOSFET driver
      break;                                                                                  
      
    default:                                                                                  // if none of the other cases are satisfied,
      disable_charger();                                                                      // Disable the MOSFET driver
      break;
  }
}

float targetCurrent=20;
float targetVolt=5;
float KP=10,KD=0.6;
float lastError=0,trackVal;

void PerturbAndObserve2(){
  
  float error = targetVolt - sol_amps;
  int trackVal = KP * error + KD * (error - lastError);  //kp=0.02,kd=0.4,ki=0.08,190-200
  lastError = error;
  pulseWidth = pulseWidth + trackVal;
  
  
  
 
  
  if (pulseWidth < 200)
    pulseWidth = 200;

  if (pulseWidth > 800)
    pulseWidth = 800;
                                                                         // add (or subtract) track Direction to(from) pulseWidth
}

void PerturbAndObserve(){
  if(bat_amps < MAX_BAT_CURRENT){
  if ((pulseWidth == 300) || (pulseWidth == 800) || (sol_watts < old_sol_watts)) trackDirection = -trackDirection;  // if pulseWidth has hit one of the ends reverse the track direction
  pulseWidth = pulseWidth + trackDirection;

  } 
  
  else{
  float error = MAX_BAT_CURRENT - bat_amps;
  int trackVal = KP * error + KD * (error - lastError);  //kp=0.02,kd=0.4,ki=0.08,190-200
  lastError = error;
  pulseWidth = pulseWidth + trackVal;
  
  }
  
 
  
  if (pulseWidth < 300)
    pulseWidth = 300;

  if (pulseWidth > 800)
    pulseWidth = 800;
                                                                         // add (or subtract) track Direction to(from) pulseWidth
}

void enable_charger() {
  pulseWidth = constrain (pulseWidth, 200, 800);               // prevent overflow of pulseWidth and not fully on or off for the charge pump
  pwm = map(pulseWidth, 0, 1024, 0, 1024);                       // use pulseWidth to get a % value and store it in pwm
  Timer1.pwm(PWM_PIN, pulseWidth, 20);                          // use Timer1 routine to set pwm duty cycle at 20uS period
  TURN_ON_MOSFETS;            // enable the MOSFET driver                                 
}

void disable_charger() {
  TURN_OFF_MOSFETS;                                             // disable MOSFET driver
} 

//----------------------------------------------------------------------------------------------------------------------
/////////////////////////////////////////////LOAD CONTROL/////////////////////////////////////////////////////
//----------------------------------------------------------------------------------------------------------------------  
  
void load_control(){
  if ((sol_watts < 1) && (bat_volts > (LVD - deltaV))){            // If the panel isn't producing, it's probably night
    load_status = 1;                                               // record that the load is on
  }
  else if (bat_volts < (LVD - deltaV)){                            // If the battery voltage drops below the low voltage threshold
    digitalWrite(LOAD_PIN, HIGH);                                  // turn the load off
    load_status = 0;                                               // record that the load is off
  }  
  else if (sol_watts > 1){                                         // If the panel is producing, it's day time
    digitalWrite(LOAD_PIN, HIGH);                                  // turn the load off
    load_status = 0;                                               // record that the load is off
  }
  if ((load_status = 1) && (prev_load_status = 0)){                // If the load status has changed from off to on
    digitalWrite(LOAD_PIN, LOW);                                   // turn the load on
    loaded_volts = read_adc(BAT_VOLTS_CHAN) * BAT_VOLTS_SCALE;     // input of battery volts under load
    deltaV = (bat_volts - loaded_volts);                           // record the difference in voltage
  }
  prev_load_status = load_status;                                  // save the load status
}

//------------------------------------------------------------------------------------------------------
// This routine prints all the data out to the serial port.
//------------------------------------------------------------------------------------------------------
void print_data(void) {
  
  // Serial.print(seconds,DEC);
  // Serial.print("      ");

 //no_battery, sleep, bulk, Float, error
  Serial.print("Charging = ");
  if (charger_state == no_battery) Serial.print("noBat");
  else if (charger_state == sleep) Serial.print("sleep");
  else if (charger_state == bulk) Serial.print("bulk");
  else if (charger_state == Float) Serial.print("float");
  else if (charger_state == error) Serial.print("error");
  Serial.print("    ");

  Serial.print("pwm = ");
  Serial.print(pwm,DEC);
  Serial.print("    ");

  Serial.print("Current (panel) = ");
  //print_int100_dec2(sol_amps);
  Serial.print(sol_amps);
  Serial.print("    ");

  Serial.print("Voltage (panel) = ");
  Serial.print(sol_volts);
  //print_int100_dec2(sol_volts);
  Serial.print("    ");

  Serial.print("Power (panel) = ");
  Serial.print(sol_watts);
  // print_int100_dec2(sol_watts);
  Serial.print("    ");

  Serial.print("Battery Voltage = ");
  Serial.print(bat_volts);
  //print_int100_dec2(bat_volts);
  Serial.print("    ");

  Serial.print("Battery Current = ");
  Serial.print(bat_amps);
  //print_int100_dec2(bat_volts);
  Serial.print("    ");

  Serial.print("Power (output) = ");
  Serial.print(output_watts);
  // print_int100_dec2(sol_watts);
  Serial.print("    ");

  Serial.print("\n\r");
  //delay(1000);
}

//-------------------------------------------------------------------------------------------------
//---------------------------------Led Indication--------------------------------------------------
//-------------------------------------------------------------------------------------------------

void led_output(void)
{
  if(bat_volts > 14.1 )
  {
      leds_off_all();
      digitalWrite(LED_YELLOW, HIGH); 
  } 
  else if(bat_volts > 11.9 && bat_volts < 14.1)
  {
      leds_off_all();
      digitalWrite(LED_GREEN, HIGH);
  } 
  else if(bat_volts < 11.8)
  {
      leds_off_all;
      digitalWrite(LED_RED, HIGH); 
  } 
  
}

//------------------------------------------------------------------------------------------------------
//
// This function is used to turn all the leds off
//
//------------------------------------------------------------------------------------------------------
void leds_off_all(void)
{
  digitalWrite(LED_GREEN, LOW);
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_YELLOW, LOW);
}

//------------------------------------------------------------------------------------------------------
//-------------------------- LCD DISPLAY --------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------

int intefficiency;

void lcd_display()
{
  
  lastUpdatetime = millis();                        // If any of the buttons are pressed, save the time in millis to "time"
  

  //lcd.clear();

  if(lcdCounter == 0){
    lcd.setCursor(0,0);
    lcd.print("P:");
    lcd.print(sol_watts);
    lcd.print("W "); 
    if (charger_state == no_battery) 
   lcd.print("no batt");
   else if (charger_state == sleep)
   lcd.print("sleep");
   else if (charger_state == bulk)
   lcd.print("bulk");
   else if (charger_state == Float)
   lcd.print("float");
   else if (charger_state == error)
   lcd.print("error");

   lcd.setCursor(0,1);
   //displaymAh = (int) calmAh;

   lcd.print(displaymAh);
   lcd.print( " ");
   // if ( bat_volts >= 12.7)
   // lcd.print( "100%");
   // else if (bat_volts >= 12.5 && bat_volts < 12.7)
   // lcd.print( "90%");
   // else if (bat_volts >= 12.42 && bat_volts < 12.5)
   // lcd.print( "80%");
   // else if (bat_volts >= 12.32 && bat_volts < 12.42)
   // lcd.print( "70%");
   // else if (bat_volts >= 12.2 && bat_volts < 12.32)
   // lcd.print( "60%");
   // else if (bat_volts >= 12.06 && bat_volts < 12.2)
   // lcd.print( "50%");
   // else if (bat_volts >= 11.90 && bat_volts < 12.06)
   // lcd.print( "40%");
   // else if (bat_volts >= 11.75 && bat_volts < 11.90)
   // lcd.print( "30%");
   // else if (bat_volts >= 11.58 && bat_volts < 11.75)
   // lcd.print( "20%");
   // else if (bat_volts >= 11.31 && bat_volts < 11.58)
   // lcd.print( "10%");
   // else if (bat_volts < 11.3)
   // lcd.print( "0%");

   lcd.print("   E:");
   intefficiency = efficiency;
   lcd.print(intefficiency);
   lcd.print( "%");
  }

  if(lcdCounter == 1){
   lcd.setCursor(0,0);
   lcd.print("S:");
   lcd.print(sol_volts);
   lcd.print("V "); 
   lcd.print(sol_amps);
   lcd.print("A"); 
   lcd.setCursor(0,1);
   lcd.print("B:");
   lcd.print(bat_volts);
   lcd.print("V "); 
   lcd.print(bat_amps);
   lcd.print("A");   

  }
 
            // call the backlight timer function in every loop 
}

void lcd_timer(){
  if((millis() - lastUpdatetime) > 500){         // if it's been less than the 15 secs, turn the backlight on
      lcd_display();                  // finish with backlight on  
                 // if it's been more than 15 secs, turn the backlight off
      digitalWrite(LED_RED, ledState);
      ledState = ~ledState;

    }
}



int i2c_write_and_verify (unsigned char reg_addr, unsigned char data, i2c_trans *i2c_write) {
    // =========================================================================
    // I2C Write & Verify
    // Write data from MSP430 to a bq76925 register then read-back to verify.
    // This function performs two I2C transactions. The first transaction writes
    // 'data' to the address 'reg_addr'. The second transaction reads the
    // address 'reg_addr' and compares the read data against the write data.
    //
    // Arguments
    //     reg_addr     = Combined address of bq76925 register formed by
    //                    shifting the group address left by 3 and adding the
    //                    base register address
    //     data         = Data to be written to bq76925 register
    //     i2c_write    = Pointer to data structure for storing write data
    //
    // Return value     = Error code:
    //                        0 = no error
    //                        1 = error during write transaction 
    //                        2 = error during read transaction
    //                        3 = write and read data mismatch
    // =========================================================================
    Wire.beginTransmission(reg_addr);
    Wire.write(data);
    Wire.endTransmission();
    // Declare local read transaction struct for data read-back
    i2c_trans i2c_read_struct;
    
  
    
    if (i2c_read(reg_addr, &i2c_read_struct)) {
        // Error during read-back transaction. Fill write transaction data
        // with 0. Set error status to indicate error during read-back.
        (*i2c_write).data =  0;
        (*i2c_write).error = 2;
    } else {
        // Read-back completed without an error. Fill write transaction data
        // with read-back data.
        (*i2c_write).data = i2c_read_struct.data;
        
        // Check that the read-back data matches the write data
        if (i2c_read_struct.data != data) {
          Serial.print("Sent ");
          Serial.print(data);
          Serial.print("Received ");
          Serial.print(i2c_read_struct.data);

            // Read data does not match write data. Set error status to
            // indicate data mismatch.    
          Serial.println("mismatch");
            (*i2c_write).error = 3;
        } else {
            // Set error status to indicate no error.
            (*i2c_write).error = 0;
        }
    }

    // Return error status
    return (*i2c_write).error;
}


int i2c_read(unsigned char reg_addr, i2c_trans *i2c_read) {
  Wire.beginTransmission(reg_addr);
  Wire.endTransmission();
  Wire.requestFrom(reg_addr,1);
  (*i2c_read).data  = Wire.read();
  (*i2c_read).error = 0;

return (*i2c_read).error;
}


void updateBatCurrent(){
  i2c_trans i2c_read_struct, i2c_write_struct;
    // Measure current
  // ---------------
  // Measure SENSEP via VIOUT and then calculate result using SENSEN
  // measurement taken above.
  adc_data = analogRead(VIOUT_PIN);

  // Check for discharge current
  if (adc_data <= adc_sensen) {
      // If current is discharge, take difference of SENSEN and SENSEP,
      // multiply ADC count by vref_corr and divide by 256 to give 16-bit
      // result.
      adc_data = (adc_sensen - adc_data) ;
      Serial.print("current ");
      Serial.println(adc_data);
      // Determine if fault threshold is violated by comparing
      // the result to the over current threshold voltage
      if (adc_data > VI_TRANS(OC_THRESH)) {
          // Clear P1.1 if over current fault detected
            
      } else {
          // Set P1.1 if fault is not detected
          
      } 
  }
}

void updateBatTemp(){
  i2c_trans i2c_read_struct, i2c_write_struct;
    // Measure temperature
// -------------------
// Activate thermistor bias on VTB pin by setting enable bit in
// bq76925 POWER_CTL register.
if (i2c_write_and_verify(POWER_CTL, 0x0F, &i2c_write_struct)) error_trap();

// Delay 32 ms before taking measurement. Delay takes into account the
// time required to charge up any capacitance in thermistor network and
// should be adjusted according to circuit values. 
delay(100);

// Multiply ADC count by vref_corr and divide by 256 to give a
// 16-bit result 
adc_data = analogRead(THERM_PIN) ;

// Disable thermistor bias on VTB pin
if (i2c_write_and_verify(POWER_CTL, 0x0D, &i2c_write_struct)) error_trap();

// Determine if fault threshold is violated by comparing the adc_data
// output to the over temperature voltage threshold
if (adc_data < VT_TRANS(OT_THRESH)) {
    // Clear P1.0 if over temperature fault detected
       
} else {
    // Set P1.0 if fault is not detected

} 

}

void updateCellVoltages(){
  i2c_trans i2c_read_struct, i2c_write_struct;
  StackVoltage = 0;
  for (int i = 0; i < NUM_CELLS; i++)
  {
    byte tempAddress = 0x10 + i;
    int temp = 0;
    i2c_write_and_verify(CELL_CTL,tempAddress,&i2c_write_struct);
    for (int j = 0; j < Vol_avg_count; j++)
    {
      temp = temp + analogRead(VCOUT_PIN);
    }


    Cell_Vol[i] = 1000.0*temp/ADC_FULL_COUNT/GVCOUT*ADC_VREF_NOM/Vol_avg_count;
  }
  for (int i = 0; i < NUM_CELLS; ++i)
  {
    StackVoltage = StackVoltage + Cell_Vol[i];
   // Serial.print(Cell_Vol[i]);
   // Serial.print(" ");
  }
 // Serial.println(StackVoltage);
    // After checking all cells, set the fault indicators
  if (uv_flag) {
      // Clear P1.2 if UV fault detected
      
  } else {
      // Set P1.2 if fault is not detected
      
  }        
   
  if (ov_flag) {
      // Clear P1.3 if OV fault detected
      
  } else {
      // Set P1.3 if fault is not detected
     
  }        

}

void startBalancing(byte cellNum){
  i2c_trans i2c_read_struct, i2c_write_struct;
  i2c_write_and_verify(BAL_CTL,cellNum,&i2c_write_struct);
}

void stopBalancing(){

 
}
/// B0101 1111

void error_trap (void) {
    // =========================================================================
    // Error Trap
    // This error handling routine is simply an infinite loop. The device will
    // reset once the watchdog timer expires. In an actual application this
    // would be filled in with code appropriate for the application.
    // =========================================================================
    Serial.println("Error");
}